import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';

import 'flutter_fayaalma_checkout_platform_interface.dart';

/// An implementation of [FlutterFayaalmaCheckoutPlatform] that uses method channels.
class MethodChannelFlutterFayaalmaCheckout extends FlutterFayaalmaCheckoutPlatform {
  /// The method channel used to interact with the native platform.
  @visibleForTesting
  final methodChannel = const MethodChannel('flutter_fayaalma_checkout');

  @override
  Future<String?> getPlatformVersion() async {
    final version = await methodChannel.invokeMethod<String>('getPlatformVersion');
    return version;
  }
}
