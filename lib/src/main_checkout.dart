import 'package:flutter_easyloading/flutter_easyloading.dart' as es_load;
import '../flutter_fayaalma_checkout.dart';
import 'widgets/fayaalma_stpper.dart';

// ignore: must_be_immutable
class FayaalmaCheckout extends StatelessWidget {
  CheckOutEntity initCheckOutEntity;
  double? width, height;
  TextStyle? styleTitle, styleSubTitle;
  Color? actifColor, inactifColor;
  FayaalmaCheckout({
    super.key,
    required this.initCheckOutEntity,
    this.width = double.infinity,
    this.height = double.infinity,
    this.styleTitle,
    this.styleSubTitle,
    this.actifColor,
    this.inactifColor,
  });

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      builder: es_load.EasyLoading.init(),
      home: SizedBox(
        width: width,
        height: height,
        child: FayaalmaStepper(
          initCheckOutEntity: initCheckOutEntity,
          styleTitle: styleTitle,
          styleSubTitle: styleSubTitle,
          actifColor: actifColor,
          inactifColor: inactifColor,
        ),
      ),
    );
  }
}
