class MainResponseModel {
  bool? status;
  String? message;
  dynamic data;

  MainResponseModel(
      {this.status,
      this.message,
      this.data,
      });

  MainResponseModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    data = json['data'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['status'] = status;
    data['message'] = message;
    if (this.data != null) {
      data['data'] = this.data;
    }
    return data;
  }
}
