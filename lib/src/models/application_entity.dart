class ApplicationEntity {
  String? appName, appLogo;

  ApplicationEntity({this.appName, this.appLogo});

  ApplicationEntity.fromJson(Map<String, dynamic> json) {
    appName = json['appName'];
    appLogo = json['appLogo'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['appName'] = appName;
    data['appLogo'] = appLogo;
    return data;
  }
}