class ProductEntity {
  String productName;
  double unitPrice;
  int quantity;

  // Constructeur
  ProductEntity({
    required this.productName,
    required this.unitPrice,
    required this.quantity,
  });

  // Méthode pour convertir un objet ProductEntity en un Map
  Map<String, dynamic> toMap() {
    return {
      'productName': productName,
      'unitPrice': unitPrice,
      'quantity': quantity,
    };
  }

  // Méthode pour créer un objet ProductEntity à partir d'un Map
  static ProductEntity fromMap(Map<String, dynamic> map) {
    return ProductEntity(
      productName: map['productName'].toString(),
      unitPrice: double.tryParse(map['unitPrice'].toString())?? 0.0,
      quantity: int.tryParse(map['quantity'].toString())?? 0,
    );
  }
}

