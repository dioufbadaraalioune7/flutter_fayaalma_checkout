import 'product_entity.dart';

class CheckOutEntity {
  String appID;
  String orderID;
  double amount;
  String? payerFullName;
  List<ProductEntity>? productsOrdered;

  // Constructeur
  CheckOutEntity({
    required this.appID,
    required this.orderID,
    required this.amount,
    this.payerFullName,
    this.productsOrdered,
  });

  // Méthode pour convertir un objet CheckOutEntity en un Map
  Map<String, dynamic> toMap() {
    return {
      'appID': appID,
      'orderID': orderID,
      'amount': amount,
      'payerFullName': payerFullName,
      'fromPlugs': true,
      // ignore: prefer_null_aware_operators
      'orderDetails': productsOrdered != null
          ? productsOrdered!.map((product) => product.toMap()).toList()
          : null,
    };
  }

  // Méthode pour créer un objet CheckOutEntity à partir d'un Map
  static CheckOutEntity fromMap(Map<String, dynamic> map) {
    return CheckOutEntity(
      appID: map['appID'].toString(),
      orderID: map['orderID'].toString(),
      amount: double.tryParse(map['amount'].toString())?? 0.0,
      payerFullName: map['payerFullName'].toString(),
      productsOrdered: map['orderDetails'] != null
          ? List<ProductEntity>.from(
              map['orderDetails'].map((productMap) =>
                  ProductEntity.fromMap(productMap),
              ),
            )
          : null,
    );
  }
}

