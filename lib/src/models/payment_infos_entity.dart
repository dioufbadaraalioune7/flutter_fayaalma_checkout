import 'package:flutter_fayaalma_checkout/src/models/application_entity.dart';
import 'package:flutter_fayaalma_checkout/src/models/product_entity.dart';

class PaymentInfosEntity {
  PaymentInfos? paymentInfos;
  String? paymentCode;
  dynamic paymentQrCode;
  dynamic paymentTamplete;
  ApplicationEntity? application;
  PaymentInfosEntity(
      {this.paymentInfos,
      this.paymentCode,
      this.application,
      this.paymentQrCode,
      this.paymentTamplete});

  PaymentInfosEntity.fromJson(Map<String, dynamic> json) {
    paymentInfos = json['paymentInfos'] != null
        ? PaymentInfos.fromJson(json['paymentInfos'])
        : null;
    application = json['application'] != null
        ? ApplicationEntity.fromJson(json['application'])
        : null;
    paymentCode = json['paymentCode'];
    paymentQrCode = json['paymentQrCode'];
    paymentTamplete = json['paymentTamplete'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    if (this.paymentInfos != null) {
      data['paymentInfos'] = this.paymentInfos!.toJson();
    }
    data['paymentCode'] = this.paymentCode;
    data['paymentQrCode'] = this.paymentQrCode;
    data['paymentTamplete'] = this.paymentTamplete;
    return data;
  }
}

class PaymentInfos {
  String? sId;
  String? appID;
  double? amount;
  String? orderID;
  String? createdAt, payerFullName;
  List<ProductEntity>? orderDetails;

  PaymentInfos(
      {this.sId, this.appID, this.amount, this.orderID, this.createdAt});

  PaymentInfos.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    appID = json['appID'];
    amount = double.tryParse(json['amount'].toString());
    orderID = json['orderID'];
    createdAt = json['createdAt'];
    payerFullName = json['payerFullName'];
    if (json['orderDetails'] != null) {
      orderDetails = <ProductEntity>[];
      json['orderDetails'].forEach((v) {
        orderDetails!.add(ProductEntity.fromMap(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['appID'] = this.appID;
    data['amount'] = this.amount;
    data['orderID'] = this.orderID;
    data['createdAt'] = this.createdAt;
    return data;
  }
}
