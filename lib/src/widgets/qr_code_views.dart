// // import 'package:flutter_html/flutter_html.dart';
// import 'package:qr_flutter/qr_flutter.dart';
// import '../../flutter_fayaalma_checkout.dart';

// class QrcodeViews extends StatefulWidget {
//   CheckOutEntity initCheckOutEntity;
//   QrcodeViews({super.key, required this.initCheckOutEntity});

//   @override
//   State<QrcodeViews> createState() => _QrcodeViewsState();
// }

// class _QrcodeViewsState extends State<QrcodeViews> {
//   @override
//   void initState() {
//     generateQRCodeAndpaymentCode(widget.initCheckOutEntity);
//     super.initState();
//   }

//   generateQRCodeAndpaymentCode(CheckOutEntity checkOutEntity) {}

//   @override
//   Widget build(BuildContext context) {
//     return SizedBox(
//       width: double.infinity,
//       height: double.infinity,
//       child: Container(
//         decoration: BoxDecoration(
//           borderRadius: BorderRadius.circular(10),
//           color: Colors.blue.shade400,
//         ),
//         child: Padding(
//           padding: const EdgeInsets.all(15.0),
//           child: Column(
//             children: [
//               Expanded(
//                 flex: 2,
//                 child: Container(
//                   padding: const EdgeInsets.only(left: 40, top: 20, bottom: 20),
//                   decoration: BoxDecoration(
//                     color: Colors.white,
//                     borderRadius: BorderRadius.circular(10),
//                   ),
//                   child: QrImageView(
//                     data: 'DIT-38F-W2X',
//                     version: QrVersions.min,
//                     size: double.infinity,
//                     padding: EdgeInsets.zero,
//                   ),
//                 ),
//               ),
//               const SizedBox(height: 20),
//               Expanded(
//                 flex: 3,
//                 child: Column(
//                   children: [
//                     Container(
//                       padding: const EdgeInsets.all(20),
//                       decoration: BoxDecoration(
//                         border: Border.all(color: Colors.white, width: 0.5),
//                       ),
//                       child: const Text(
//                         "Téléchargez(ios ou android) et scannez avec l'application SAMA KALPÉ, ou cliquez sur le bouton ci-dessous pour effectuer le paiement par CODE DE PAIEMENT.",
//                         textAlign: TextAlign.justify,
//                         style: TextStyle(
//                             color: Colors.white,
//                             fontWeight: FontWeight.w500,
//                             fontSize: 17),
//                       ),
//                     )
//                   ],
//                 ),
//               )
//             ],
//           ),
//         ),
//       ),
//     );
//   }

//   // Widget html = Html(
//   //     data: """<div style='width:300px;background-color:#3498DB;padding:30px;display:flex;flex-direction:column;align-items:center;border-radius:10px;'><div style='width:100%;padding:30px;border-radius:10px;'><img style='width:100%;height:100%;border-radius:10px;' src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJQAAACUCAYAAAB1PADUAAAAAklEQVR4AewaftIAAATDSURBVO3BQY4cSRIEQdNA/f/Lun30UwCJ9GpyOSaCP1K15KRq0UnVopOqRSdVi06qFp1ULTqpWnRSteikatFJ1aKTqkUnVYtOqhadVC06qVr0yUtAfpOaN4BMaiYgT6i5AfKEmgnIb1LzxknVopOqRSdViz5ZpmYTkCeAPAHkDSBPqJmAPKFmE5BNJ1WLTqoWnVQt+uTLgDyh5gkgk5obIE+omYBMav4kIE+o+aaTqkUnVYtOqhZ98o8B8oSaGyBPAJnU/JecVC06qVp0UrXok/8YNU+omYBMQCY1E5AbNf+Sk6pFJ1WLTqoWffJlan6TmgnIBGRS84SaGyA3ajap+ZucVC06qVp0UrXok2VA/mZqJiCTmgnIpGYCMqmZgNwAmdTcAPmbnVQtOqladFK16JOX1PxNgNwAmdR8k5oJyKTmRs3/k5OqRSdVi06qFuGPvABkUjMB2aTmDSCTmjeA3Kh5A8gmNd90UrXopGrRSdWiT5YBeULNE0AmNROQJ4C8oeYGyJ+k5gbIpOaNk6pFJ1WLTqoWffKSmgnIpOYJIDdqnlBzA+QNIDdqboBMam6ATGomIDdAJjWbTqoWnVQtOqlahD/yApBJzQ2QGzU3QCY1bwC5UTMBmdTcAHlDzQ2QSc0TQCY1b5xULTqpWnRSteiTl9RMQCY1k5obIDdqboA8oeYGyBNAJjU3QCY1N0AmNROQJ9RsOqladFK16KRqEf7IHwRkUnMD5Ak1E5AbNTdAbtRMQG7UTEBu1ExAJjU3QG7UvHFSteikatFJ1SL8kReATGomIE+omYBMam6A3Kj5JiA3aiYgk5obIE+o+U0nVYtOqhadVC36ZBmQGzUTkBs1E5AbNU8AmdRMQCY1E5BJzQ2QSc0E5EbNBORvclK16KRq0UnVIvyRXwRkUnMD5EbNDZAbNZuATGpugExqboBMam6A3KjZdFK16KRq0UnVIvyRF4BMam6A3Ki5AfKEmgnIpGYC8oSaGyA3aiYgN2omIDdqJiA3at44qVp0UrXopGrRJ18GZFIzAbkBcqPmCTUTkG9S84SaN9TcqPmmk6pFJ1WLTqoWffLLgExqJiCTmgnIb1JzA2RSMwGZ1NwAmdRMQDYBmdS8cVK16KRq0UnVIvyRF4BMaiYgm9RMQCY1E5BJzQRkUjMB+Zep2XRSteikatFJ1aJPXlJzo+ZPUvOGmk1AbtQ8AWRS8wSQSc0bJ1WLTqoWnVQt+uQlIL9JzaTmBsikZlLzBJAn1LwBZFLzBJAbNZtOqhadVC06qVr0yTI1m4DcAPkmIDdqnlAzAblR84aaCcg3nVQtOqladFK16JMvA/KEmjfU3AC5UTOpmYBMQG7UPAHkDSB/0knVopOqRSdViz75xwG5UTMBuVHzBJAn1GwCcgNkUvPGSdWik6pFJ1WLPvmPUfOEmgnIjZpJzRNA3lAzqZmATGo2nVQtOqladFK16JMvU/NNam7U/ElAJjUTkEnNE0D+JidVi06qFp1ULfpkGZDfBGRScwPkRs0TaiYgk5oJyKRmAvIGkBs133RSteikatFJ1SL8kaolJ1WLTqoWnVQtOqladFK16KRq0UnVopOqRSdVi06qFp1ULTqpWnRSteikatFJ1aL/AREwMiiQXdsfAAAAAElFTkSuQmCC'></div><div style='margin-top:20px;border:0.5px white solid;padding:10px;color:white;'>Téléchargez(ios ou android) et scannez avec l'application SAMA KALPÉ, ou cliquez sur le bouton ci-dessous pour effectuer le paiement par CODE DE PAIEMENT.</div><div style='margin-top:20px;width:100%;display:flex;justify-content:space-between;'><a href='https://test.samakalpe.com/#/fayaalma/174-33E-A60' target='_blank' style='flex:1;display:flex;height:50px;background-color:white;border-radius:10px;text-decoration:none;justify-content:center;align-items:center;color:#3498DB;'><small style='font-size:10px;margin-right:5px;display:block;'>CODE PAIEMENT:</small>174-33E-A60</a></div></div>""");
// }
