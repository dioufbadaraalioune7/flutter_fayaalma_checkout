import 'package:flutter/services.dart';
import 'package:flutter_fayaalma_checkout/flutter_fayaalma_checkout.dart';
import 'package:google_fonts/google_fonts.dart';

// ignore: must_be_immutable
class InputForm extends StatelessWidget {
  Function(String?)? onChanged;
  IconData icon;
  String label;
  TextInputType? inputType;
  List<TextInputFormatter>? formatter;
  double? height;
  double marginb;
  int? maxlength;
  String? Function(String?)? validator;
  InputBorder? border;
  double fontSize;
  FontWeight? fontWeight;
  TextEditingController? controller;
  bool obscureText;

  InputForm({
    super.key,
    required this.icon,
    required this.label,
    this.onChanged,
    this.inputType,
    this.height,
    this.validator,
    this.marginb = 30,
    this.fontSize = 15,
    this.fontWeight,
    this.controller,
    this.border = const OutlineInputBorder(),
    this.formatter,
    this.obscureText = false,
    this.maxlength,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: height,
      margin: EdgeInsets.only(bottom: marginb),
      child: TextFormField(
        obscureText: obscureText,
        onChanged: onChanged,
        keyboardType: inputType,
        maxLength: maxlength,
        inputFormatters: formatter,
        validator: validator,
        controller: controller,
        cursorColor: const Color(0xFF07AEEF),
        style: GoogleFonts.lora(
          color: const Color(0xFF424949),
          fontSize: fontSize,
          fontWeight: fontWeight,
        ),
        decoration: InputDecoration(
          iconColor: const Color(0xFF07AEEF),
          labelText: label,
          labelStyle: GoogleFonts.lora(
            color: const Color(0xFF424949),
            fontSize: 15,
          ),
          suffixIcon: Icon(
            icon,
            color: const Color(0xFF07AEEF),
          ),
          border: const OutlineInputBorder(
            borderSide: BorderSide(
              color: Color(0xFF07AEEF),
              width: 0.5,
            ),
          ),
          enabledBorder: const OutlineInputBorder(
            borderSide: BorderSide(
              color: Color(0xFF07AEEF),
              width: 0.5,
            ),
          ),
          focusedBorder: const OutlineInputBorder(
            borderSide: BorderSide(
              color: Color(0xFF07AEEF),
              width: 0.5,
            ),
          ),
        ),
      ),
    );
  }
}
