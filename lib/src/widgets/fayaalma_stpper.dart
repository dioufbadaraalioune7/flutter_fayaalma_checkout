import 'package:flutter_fayaalma_checkout/src/models/payment_infos_entity.dart';
import 'package:flutter_fayaalma_checkout/src/services/main_service.dart';
import 'package:flutter_fayaalma_checkout/src/utils/form_helper.dart';
import 'package:flutter_fayaalma_checkout/src/widgets/input_form.dart';
import 'package:flutter_fayaalma_checkout/src/widgets/input_with_dropdown.dart';
import 'package:google_fonts/google_fonts.dart';
import '../../flutter_fayaalma_checkout.dart';

// ignore: must_be_immutable
class FayaalmaStepper extends StatefulWidget {
  CheckOutEntity initCheckOutEntity;
  TextStyle? styleTitle, styleSubTitle;
  Color? actifColor, inactifColor;
  FayaalmaStepper({
    super.key,
    required this.initCheckOutEntity,
    this.styleTitle,
    this.styleSubTitle,
    this.actifColor,
    this.inactifColor,
  });

  @override
  State<FayaalmaStepper> createState() => _FayaalmaStepperState();
}

class _FayaalmaStepperState extends State<FayaalmaStepper> {
  int currentStep = 2;
  MainService mainService = MainService();
  PaymentInfosEntity? paymentInfosEntity;
  TextEditingController ccontroller = TextEditingController();
  TextEditingController dcontroller = TextEditingController();

  generatePaymentInfos() async {
    var res = await mainService.doPostRequest(
        "payment-code/generates", widget.initCheckOutEntity.toMap());
    if (res.status!) {
      setState(() {
        paymentInfosEntity = PaymentInfosEntity.fromJson(res.data);
        ccontroller.text = paymentInfosEntity?.paymentCode ?? "";
      });
    }
  }

  @override
  void initState() {
    generatePaymentInfos();
    super.initState();
  }

  var boxDecoration = BoxDecoration(
    color: Colors.white,
    borderRadius: BorderRadius.circular(5),
    border: Border.all(
      width: 0.5,
      color: Colors.grey.shade400.withOpacity(0.6),
    ),
  );

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Center(
          child: Text(
            "PAGE DE PAIEMENT",
            style: GoogleFonts.lora(
              fontWeight: FontWeight.w400,
              fontSize: 16,
              color: Colors.black,
            ),
          ),
        ),
        const SizedBox(height: 20),
        Container(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          width: double.infinity,
          height: 50,
          child: Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                ItemHeadStp(
                  title: 'Étape 1',
                  subTitle: 'DETAILS COMMANDE',
                  isActif: currentStep == 1,
                  styleTitle: widget.styleTitle,
                  styleSubTitle: widget.styleSubTitle,
                  actifColor: widget.actifColor,
                  inactifColor: widget.inactifColor,
                  onTap: () {
                    setState(() {
                      currentStep = 1;
                    });
                  },
                ),
                ItemHeadStp(
                  title: 'Étape 2',
                  subTitle: 'INOS PAIEMENT',
                  isActif: currentStep == 2,
                  icon: Icons.payment,
                  styleTitle: widget.styleTitle,
                  styleSubTitle: widget.styleSubTitle,
                  actifColor: widget.actifColor,
                  inactifColor: widget.inactifColor,
                  withSeparator: false,
                  onTap: () {
                    setState(() {
                      currentStep = 2;
                    });
                  },
                ),
              ],
            ),
          ),
        ),
        const SizedBox(height: 10),
        currentStep == 2
            ? PaymentForm(
                boxDecoration: boxDecoration,
                ccontroller: ccontroller,
                dcontroller: dcontroller,
                paymentInfosEntity: paymentInfosEntity,
              )
            : PaymentInfo(
                paymentInfosEntity: paymentInfosEntity,
                boxDecoration: boxDecoration,
              ),
      ],
    );
  }
}

class PaymentInfo extends StatelessWidget {
  const PaymentInfo({super.key, this.paymentInfosEntity, this.boxDecoration});
  final PaymentInfosEntity? paymentInfosEntity;
  final BoxDecoration? boxDecoration;

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: Column(
          children: [
            for(var i =0; i<(paymentInfosEntity?.paymentInfos?.orderDetails?? []).length; i++)
              itemProct(paymentInfosEntity?.paymentInfos?.orderDetails![i])
          ],
        ),
      ),
    );
  }

  Widget itemProct(ProductEntity? productEntity) {
    return Container(
      width: double.infinity,
      height: 150,
      margin: const EdgeInsets.only(bottom: 20),
      decoration: boxDecoration,
      padding: const EdgeInsets.symmetric(
        horizontal: 20,
        vertical: 10,
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            (productEntity?.productName?? "---").toUpperCase(),
            style: GoogleFonts.lora(fontSize: 17, fontWeight: FontWeight.w700),
          ),
          const SizedBox(height: 15),
          smallInfo("Prix Unitaire", '${productEntity?.unitPrice?? 0} XOF'),
          const SizedBox(height: 10),
          smallInfo("Quantité", productEntity?.quantity?? 0),
          const SizedBox(height: 10),
          smallInfo("Prix Total", '${((productEntity?.unitPrice?? 0)* (productEntity?.quantity?? 0))} XOF'),
        ],
      ),
    );
  }

  smallInfo(label, value) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          (label).toUpperCase(),
          style: GoogleFonts.lora(
            fontSize: 12,
            fontWeight: FontWeight.w600,
            color: Colors.grey.shade500,
          ),
        ),
        Text(
          value.toString(),
          style: GoogleFonts.lora(
            fontSize: 15,
            fontWeight: FontWeight.w800,
            // color: Colors.grey.shade500,
          ),
        ),
      ],
    );
  }
}

class PaymentForm extends StatelessWidget {
  const PaymentForm({
    super.key,
    required this.boxDecoration,
    required this.ccontroller,
    required this.dcontroller,
    required this.paymentInfosEntity,
  });

  final BoxDecoration boxDecoration;
  final TextEditingController ccontroller;
  final TextEditingController dcontroller;
  final PaymentInfosEntity? paymentInfosEntity;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Column(
        children: [
          Container(
            margin: const EdgeInsets.symmetric(horizontal: 10),
            padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
            decoration: boxDecoration,
            child: Form(
              child: Column(
                children: [
                  InputForm(
                    label: "Code de Paiement",
                    controller: ccontroller,
                    inputType: TextInputType.number,
                    icon: Icons.face,
                    validator: FormValidator.requiredValidation,
                    border: const OutlineInputBorder(
                      borderSide: BorderSide(color: Color(0xFFb8e9ff)),
                    ),
                    onChanged: (value) {},
                  ),
                  DynamicDropDownInput(
                    label: 'Numéro Téléphone',
                    controller: dcontroller,
                    list: SelectModel.mobileMoneyToSelectModel(),
                    onSelectChanged: (value) {},
                  ),
                ],
              ),
            ),
          ),
          Expanded(child: Container()),
          Container(
            decoration: boxDecoration,
            width: double.infinity,
            padding: const EdgeInsets.only(
              bottom: 25,
              left: 20,
              right: 20,
              top: 10,
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      width: 50,
                      height: 50,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(30),
                        border: Border.all(
                          color: Colors.blue.shade400,
                          width: 0.5,
                        ),
                        image: const DecorationImage(
                          image: NetworkImage(
                              'https://marketplace.canva.com/EAFMNm9ybqQ/1/0/1600w/canva-gold-luxury-initial-circle-logo-qRQJCijq_Jw.jpg'),
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                    const SizedBox(width: 5),
                    Text(
                      (paymentInfosEntity?.application?.appName ?? "---")
                          .toUpperCase(),
                      style: GoogleFonts.lora(
                        fontSize: 14,
                        fontWeight: FontWeight.w400,
                        color: Colors.blue.shade400,
                      ),
                    )
                  ],
                ),
                const SizedBox(height: 10),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Montant Commande",
                      style: GoogleFonts.lora(
                        fontSize: 15,
                        fontWeight: FontWeight.w400,
                        color: Colors.black,
                      ),
                    ),
                    const SizedBox(width: 5),
                    Text(
                      '${paymentInfosEntity?.paymentInfos?.amount ?? 0.0} XOF',
                      style: GoogleFonts.lora(
                        fontSize: 15,
                        fontWeight: FontWeight.w500,
                        color: Colors.grey.shade900,
                      ),
                    ),
                  ],
                ),
                const SizedBox(height: 10),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Date Commande",
                      style: GoogleFonts.lora(
                        fontSize: 13,
                        fontWeight: FontWeight.w400,
                        color: Colors.black,
                      ),
                    ),
                    const SizedBox(width: 5),
                    Text(
                      dateFormat(paymentInfosEntity?.paymentInfos?.createdAt),
                      style: GoogleFonts.lora(
                        fontSize: 15,
                        fontWeight: FontWeight.w500,
                        color: Colors.grey.shade900,
                      ),
                    ),
                  ],
                ),
                const SizedBox(height: 10),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Nom Compléte",
                      style: GoogleFonts.lora(
                        fontSize: 13,
                        fontWeight: FontWeight.w400,
                        color: Colors.black,
                      ),
                    ),
                    const SizedBox(width: 5),
                    Text(
                      paymentInfosEntity?.paymentInfos?.payerFullName ?? "---",
                      style: GoogleFonts.lora(
                        fontSize: 15,
                        fontWeight: FontWeight.w500,
                        color: Colors.grey.shade900,
                      ),
                    ),
                  ],
                ),
                const SizedBox(height: 30),
                Container(
                  width: double.infinity,
                  height: 50,
                  decoration: BoxDecoration(
                    color: Colors.blue.shade400,
                    borderRadius: BorderRadius.circular(30),
                  ),
                  child: Center(
                    child: Text(
                      "PAYER",
                      style: GoogleFonts.lora(
                        fontSize: 15,
                        fontWeight: FontWeight.w700,
                        color: Colors.white,
                      ),
                    ),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}

// ignore: must_be_immutable
class ItemHeadStp extends StatelessWidget {
  bool isActif, withSeparator;
  IconData icon;
  String title, subTitle;
  TextStyle? styleTitle, styleSubTitle;
  Color? actifColor, inactifColor;
  void Function()? onTap;
  ItemHeadStp({
    super.key,
    required this.isActif,
    this.icon = Icons.shopping_bag,
    this.withSeparator = true,
    this.title = "Titre",
    this.subTitle = "Sous titre",
    this.styleTitle,
    this.styleSubTitle,
    this.actifColor,
    this.inactifColor,
    this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: InkWell(
        onTap: onTap,
        child: Padding(
          padding: const EdgeInsets.only(right: 0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                width: 40,
                height: 40,
                padding: const EdgeInsets.all(10),
                decoration: BoxDecoration(
                  color: isActif ? actifColor ?? Colors.blue.shade300 : null,
                  border: Border.all(
                    color: isActif
                        ? actifColor ?? Colors.blue.shade300
                        : inactifColor ?? Colors.blue.shade300,
                    width: 0.8,
                  ),
                  borderRadius: BorderRadius.circular(30),
                ),
                child: Center(
                  child: Icon(
                    isActif ? Icons.edit : icon,
                    color: isActif
                        ? Colors.white
                        : inactifColor ?? Colors.blue.shade300,
                    size: 20,
                  ),
                ),
              ),
              const SizedBox(width: 10),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    title,
                    style: styleTitle ??
                        GoogleFonts.lora(
                          fontSize: 15,
                          fontWeight: FontWeight.w400,
                          color: Colors.black,
                        ),
                  ),
                  Text(
                    subTitle,
                    overflow: TextOverflow.ellipsis,
                    style: styleSubTitle ??
                        GoogleFonts.lora(
                          fontSize: 12,
                          fontWeight: FontWeight.w300,
                          color: Colors.grey.shade500,
                        ),
                  ),
                ],
              ),
              withSeparator
                  ? Expanded(
                      child: Center(
                        child: Container(
                          height: 0.5,
                          margin: const EdgeInsets.only(left: 0),
                          color: Colors.grey.shade600,
                        ),
                      ),
                    )
                  : Container(),
            ],
          ),
        ),
      ),
    );
  }
}
