// ignore: must_be_immutable
import 'package:flutter/material.dart';
import 'package:flutter_fayaalma_checkout/src/utils/form_helper.dart';
import 'package:flutter_fayaalma_checkout/src/widgets/input_form.dart';
import 'package:google_fonts/google_fonts.dart';

// ignore: must_be_immutable
class DynamicDropDownInput extends StatefulWidget {
  List<SelectModel> list;
  String? Function(String?)? validator;
  Function(String)? onSelectChanged;
  Function(String?)? onInputChanged;
  String label;
  var inputValue = '0';
  String? defaultValue;
  bool enabled;
  Color border;
  Function(bool)? useMyPhone;
  TextEditingController? controller;
  DynamicDropDownInput({
    super.key,
    required this.list,
    required this.label,
    this.validator = FormValidator.requiredValidation,
    this.onSelectChanged,
    this.onInputChanged,
    this.defaultValue,
    this.enabled = true,
    this.border = Colors.grey,
    this.useMyPhone,
    this.controller,
  });

  @override
  State<DynamicDropDownInput> createState() => _DynamicDropDownInputState();
}

class _DynamicDropDownInputState extends State<DynamicDropDownInput> {
  var dropdownValue = "";

  @override
  Widget build(BuildContext context) {
    dropdownValue = widget.list.first.value.toString();
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        const SizedBox(height: 10),
        Container(
          height: 60,
          margin: const EdgeInsets.only(bottom: 30),
          padding: const EdgeInsets.symmetric(horizontal: 0),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5),
            border: Border.all(color: const Color(0xFFb8e9ff)),
          ),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                flex: 4,
                child: DropdownButtonFormField<String>(
                  padding: const EdgeInsets.only(left: 5),
                  value: dropdownValue,
                  validator: widget.validator,
                  enableFeedback: true,
                  focusColor: Colors.transparent,
                  dropdownColor: Colors.white,
                  icon: const Icon(
                    Icons.arrow_drop_down,
                    color: Color(0xFF07AEEF),
                    size: 20,
                  ),
                  iconSize: 20,
                  style: GoogleFonts.lora(
                    fontSize: 15,
                    color: Colors.black,
                  ),
                  onChanged: (newValue) {
                    if (widget.onSelectChanged != null) {
                      widget.onSelectChanged!(newValue!);
                    }
                  },
                  items: widget.list.map((item) {
                    return DropdownMenuItem<String>(
                      value: item.value.toString(),
                      enabled: widget.enabled,
                      child: Padding(
                        padding: const EdgeInsets.only(left: 0),
                        child: Row(
                          children: [
                            item.image != ''
                                ? Container(
                                    width: 30,
                                    height: 30,
                                    decoration: BoxDecoration(
                                      image: DecorationImage(
                                        image: AssetImage(item.image),
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                  )
                                : Container(),
                            SizedBox(
                              width: 130,
                              child: Text(
                                item.label,
                                style: GoogleFonts.lora(),
                                overflow: TextOverflow.ellipsis,
                              ),
                            )
                          ],
                        ),
                      ),
                    );
                  }).toList(),
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    focusedBorder: InputBorder.none,
                    enabledBorder: InputBorder.none,
                    errorBorder: InputBorder.none,
                    disabledBorder: InputBorder.none,
                    contentPadding: const EdgeInsets.all(0),
                    hintText: widget.label,
                    hintStyle: GoogleFonts.lora(
                      color: Colors.black,
                      fontSize: 13,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                ),
              ),
              Expanded(
                flex: 5,
                child: Padding(
                  padding: const EdgeInsets.only(top: 0),
                  child: InputForm(
                    controller: widget.controller,
                    label: widget.label,
                    height: double.infinity,
                    marginb: 0,
                    inputType: TextInputType.number,
                    icon: Icons.phone,
                    border: InputBorder.none,
                    validator: FormValidator.simplePhoneNumberValidation,
                    onChanged: widget.onInputChanged,
                    fontSize: 13,
                    fontWeight: FontWeight.w400,
                  ),
                ),
              )
            ],
          ),
        ),
      ],
    );
  }
}

class SelectModel {
  String label, image;
  dynamic value;
  SelectModel({required this.label, this.value, this.image = ''});

  static List<SelectModel> mobileMoneyToSelectModel() {
    return [
      SelectModel(label: 'Séléctionez Mobile money', image: '', value: ''),
      SelectModel(
        label: 'WAVE',
        image: '', // assets/images/mobileMoney/icon_wave.png
        value: '1',
      ),
      SelectModel(
        label: 'Orange Money',
        image: '', // assets/images/mobileMoney/icon_orange_money.png
        value: '2',
      ),
    ];
  }
}
