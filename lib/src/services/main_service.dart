import 'package:dio/dio.dart' as f_dio;
import 'package:flutter_easyloading/flutter_easyloading.dart' as es_load;
import 'package:flutter_fayaalma_checkout/src/models/main_response_model.dart';

class MainService {
  final String apiUrl = "https://api-fayaalma.daaratech.net/fylm-api/v1";
  final f_dio.Dio _dio = f_dio.Dio();
  f_dio.RequestOptions? rOptions;

  bool isContainsPath(String path) {
    return path.contains('/customers/signUp') ||
        path.contains('/customers/login') ||
        path.contains('/customers/recovAccount') ||
        path.contains('/customers/otp-validate');
  }

  MainService() {
    _dio.interceptors.add(f_dio.InterceptorsWrapper(
      onRequest: (options, handler) async {
        es_load.EasyLoading.show(status: "CHARGEMENT EN COURS ...");
        if (!isContainsPath(options.path)) {}
        return handler.next(options);
      },
      onResponse: (response, handler) {
        es_load.EasyLoading.dismiss();
        handler.next(response);
      },
      onError: (error, handler) async {
        es_load.EasyLoading.dismiss();
        es_load.EasyLoading.showError(getErrorMessage(error.response?.data['message'] ?? "UNKNOW_ERROR_CODE"));
        if ((error.response?.statusCode == 401)) {}
        return handler.next(error);
      },
    ));
  }

  Future<f_dio.Response<dynamic>> retry(f_dio.RequestOptions requestOptions) async {
    final options = f_dio.Options(
      method: requestOptions.method,
      headers: requestOptions.headers,
    );
    return _dio.request<dynamic>(requestOptions.path,
        data: requestOptions.data,
        queryParameters: requestOptions.queryParameters,
        options: options,);
  }

  // --------------- REQUEST HELPER ---------------------------------------------------------
  Future<MainResponseModel> doFormDataPostRequest(String api, f_dio.FormData data) async {
    try {
      var res = await _dio.post('$apiUrl/$api', data: data);
      return MainResponseModel.fromJson(res.data);
    } on f_dio.DioException {
      return MainResponseModel(status: false);
    }
  }

  Future<MainResponseModel> doPostRequest(String api, Map<String, dynamic> data,
      {isFylm = false}) async {
    try {
      var res = await _dio.post('$apiUrl/$api', data: data);
      return MainResponseModel.fromJson(res.data);
    } on f_dio.DioException catch (e) {
      // print(e.response?.data);
      return MainResponseModel(
          status: false, message: e.response?.data['message']);
    }
  }

  Future<MainResponseModel> doGetRequest(String api, {params, data, isFylm = false}) async {
    try {
      var res = await _dio.get('$apiUrl/$api', data: data, queryParameters: params);
      return MainResponseModel.fromJson(res.data);
    } on f_dio.DioException {
      return MainResponseModel(status: false);
    }
  }

  Future<MainResponseModel> doPatchRequest(String api, Map<String, dynamic> data) async {
    try {
      var res = await _dio.patch('$apiUrl/$api', data: data);
      return MainResponseModel.fromJson(res.data);
    } on f_dio.DioException {
      return MainResponseModel(status: false);
    }
  }

  Future<MainResponseModel> doPutRequest(String api, Map<String, dynamic> data) async {
    try {
      var res = await _dio.put('$apiUrl/$api', data: data);
      return MainResponseModel.fromJson(res.data);
    } on f_dio.DioException {
      return MainResponseModel(status: false);
    }
  }
}

String getErrorMessage(String messageCode) {
  String msg = '';
  switch (messageCode) {
    case 'INCOMPLETE_INFORMATIONS':
      msg = "Les informations que vous avez fournit sont incomplet";
      break;
    case 'INACTIVE_ACCOUNT':
      msg = "Votre compte est inactif merci de l'activer";
      break;
    case 'ACCOUNT_ALREADY_EXIST':
      msg = "Ce compte existe dejas";
      break;
    case 'ACCOUNT_NOT_EXIST':
      msg = "Ce compte n'existe pas merci de vous inscrire";
      break;
    case 'SUCCESSFULY_CONNECTED':
      msg = "Connecter avec success";
      break;
    case 'TOKEN_IS_EXPIRED':
      msg = "Session expirée ! Reconnectez-vous.";
      break;
    case 'UNKNOW_ERROR_CODE':
      msg = "Une erreur c'est produit";
      break;
    default:
      msg = messageCode;
  }
  return msg;
}
