import 'package:intl/intl.dart';

class FormValidator {
  static String? requiredValidation(String? value) {
    if ((value != null && value.isEmpty)) {
      return 'Ce champs est aubligatoire.';
    }
    return null;
  }

  static String? nameValidation(String? value) {
    if ((value != null && value.isEmpty)) {
      return 'Ce champ est obligatoire.';
    }

    if (value != null &&
        !RegExp(r'^[A-Za-zÀ-ÖØ-öø-ÿ\s]{2,}$').hasMatch(value)) {
      return 'La valeur saisie est invalide.';
    }

    return null;
  }

  static String? emailValidation(String? value) {
    if (value == null || value.isEmpty) {
      return 'Ce champs est aubligatoire.';
    }
    if (!RegExp(r'^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$').hasMatch(value)) {
      return 'L\'email n\'est pas valide.';
    }
    return null;
  }

  // static FutureOr<String>? phoneNumberValidation(PhoneNumber? value) {
  //   if (value == null || value.completeNumber.isEmpty) {
  //     return 'Ce champs est aubligatoire.';
  //   }
  //   if (!RegExp(r'^(77|76|70|75|78)[0-9]{7}$').hasMatch(value.completeNumber)) {
  //     return 'Le numéro de téléphone n\'est pas valide.';
  //   }
  //   return null;
  // }

  static String? simplePhoneNumberValidation(String? value) {
    if (value == null || value.isEmpty) {
      return 'Ce champs est aubligatoire.';
    }
    if (!RegExp(r'^(77|76|70|75|78)[0-9]{7}$').hasMatch(value)) {
      return 'Le numéro de téléphone n\'est pas valide.';
    }
    return null;
  }

  static String? passwordValidation(String? value) {
    if (value == null || value.isEmpty) {
      return 'Ce champs est aubligatoire.';
    }
    if (value.length < 4) {
      return 'Le mot de passe doit contenir au moins 4 caractères.';
    }
    return null;
  }

  static String? codeSecretValidation(String? value) {
    if (value == null || value.isEmpty) {
      return 'Ce champs est aubligatoire.';
    }
    if (value.length < 4) {
      return 'Le Code secret doit contenir 4 chiffre';
    }
    return null;
  }
}

dateFormat(String? date) {
  if (date == null) {
    return "";
  }
  return '${date.substring(0, 10)} à ${date.substring(11, 16)}';
}

String formatAmount(double amount) {
    final oCcy = NumberFormat("#,##0", "fr_FR");
    return oCcy.format(amount);
  }
