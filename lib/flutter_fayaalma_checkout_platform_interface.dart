import 'package:plugin_platform_interface/plugin_platform_interface.dart';

import 'flutter_fayaalma_checkout_method_channel.dart';

abstract class FlutterFayaalmaCheckoutPlatform extends PlatformInterface {
  /// Constructs a FlutterFayaalmaCheckoutPlatform.
  FlutterFayaalmaCheckoutPlatform() : super(token: _token);

  static final Object _token = Object();

  static FlutterFayaalmaCheckoutPlatform _instance = MethodChannelFlutterFayaalmaCheckout();

  /// The default instance of [FlutterFayaalmaCheckoutPlatform] to use.
  ///
  /// Defaults to [MethodChannelFlutterFayaalmaCheckout].
  static FlutterFayaalmaCheckoutPlatform get instance => _instance;

  /// Platform-specific implementations should set this with their own
  /// platform-specific class that extends [FlutterFayaalmaCheckoutPlatform] when
  /// they register themselves.
  static set instance(FlutterFayaalmaCheckoutPlatform instance) {
    PlatformInterface.verifyToken(instance, _token);
    _instance = instance;
  }

  Future<String?> getPlatformVersion() {
    throw UnimplementedError('platformVersion() has not been implemented.');
  }
}
