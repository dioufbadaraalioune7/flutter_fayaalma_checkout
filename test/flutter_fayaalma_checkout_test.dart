import 'package:flutter_test/flutter_test.dart';
// import 'package:flutter_fayaalma_checkout/flutter_fayaalma_checkout.dart';
import 'package:flutter_fayaalma_checkout/flutter_fayaalma_checkout_platform_interface.dart';
import 'package:flutter_fayaalma_checkout/flutter_fayaalma_checkout_method_channel.dart';
import 'package:plugin_platform_interface/plugin_platform_interface.dart';

class MockFlutterFayaalmaCheckoutPlatform
    with MockPlatformInterfaceMixin
    implements FlutterFayaalmaCheckoutPlatform {

  @override
  Future<String?> getPlatformVersion() => Future.value('42');
}

void main() {
  final FlutterFayaalmaCheckoutPlatform initialPlatform = FlutterFayaalmaCheckoutPlatform.instance;

  test('$MethodChannelFlutterFayaalmaCheckout is the default instance', () {
    expect(initialPlatform, isInstanceOf<MethodChannelFlutterFayaalmaCheckout>());
  });

  test('getPlatformVersion', () async {
    // FlutterFayaalmaCheckout flutterFayaalmaCheckoutPlugin = FlutterFayaalmaCheckout();
    MockFlutterFayaalmaCheckoutPlatform fakePlatform = MockFlutterFayaalmaCheckoutPlatform();
    FlutterFayaalmaCheckoutPlatform.instance = fakePlatform;
    // expect(await flutterFayaalmaCheckoutPlugin.getPlatformVersion(), '42');
  });
}
