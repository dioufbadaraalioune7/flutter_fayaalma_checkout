import 'package:flutter_fayaalma_checkout/flutter_fayaalma_checkout.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});
  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.grey.shade100,
        body: Padding(
          padding: const EdgeInsets.only(top: 55),
          child: FayaalmaCheckout(
            width: double.infinity,
            height: double.infinity,
            actifColor: Colors.green.shade600,
            inactifColor: Colors.amber,
            initCheckOutEntity: CheckOutEntity(
              appID: '3daf232f-97d6-4452-bd49-69e0bc793c55',
              orderID: '28430',
              amount: 100.5,
              payerFullName: 'Alioune Badara DIOUF',
              productsOrdered: [
                ProductEntity(
                  productName: "Sac En Cuire",
                  unitPrice: 200.0,
                  quantity: 1,
                ),
                ProductEntity(
                  productName: "Iphone 15 Pro",
                  unitPrice: 200.0,
                  quantity: 2,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

// fvm flutter run -t example/lib/main.dart
